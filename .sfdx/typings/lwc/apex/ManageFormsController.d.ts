declare module "@salesforce/apex/ManageFormsController.saveAll" {
  export default function saveAll(param: {param: any}): Promise<any>;
}
declare module "@salesforce/apex/ManageFormsController.getAllAccounts" {
  export default function getAllAccounts(): Promise<any>;
}
declare module "@salesforce/apex/ManageFormsController.getAccounts" {
  export default function getAccounts(param: {param: any}): Promise<any>;
}
declare module "@salesforce/apex/ManageFormsController.getId" {
  export default function getId(param: {accId: any}): Promise<any>;
}
