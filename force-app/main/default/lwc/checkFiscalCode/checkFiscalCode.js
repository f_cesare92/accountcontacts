function fiscalCodeValidate(cf){
    let validi, i, s, set1, set2, setpari, setdisp;
    if( cf == '' )  return '';
    let cf1 = cf.toUpperCase();
    if( cf1.length != 16 )
        return false;
    validi = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for( i = 0; i < 16; i++ ){
        if( validi.indexOf( cf1.charAt(i) ) == -1 )
            return false;
    }
    set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
    setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
    s = 0;
    for( i = 1; i <= 13; i += 2 )
        s += setpari.indexOf( set2.charAt( set1.indexOf( cf1.charAt(i) )));
    for( i = 0; i <= 14; i += 2 )
        s += setdisp.indexOf( set2.charAt( set1.indexOf( cf1.charAt(i) )));
    if( s%26 != cf1.charCodeAt(15)-'A'.charCodeAt(0) )
        return false;
    return true;
}

function checkFirstName(firstNameUser, fiscalCode) {
    var firstName = firstNameUser.split("");
    var found = [];
    var firstNameFC = fiscalCode.substring(3, 6).split("");
    firstName.forEach(element => {
        if(firstNameFC.includes(element.toUpperCase())){
            found.push(element);
        }
    });
    if(found.length>3 || found.length<3){
        throw "FirstName is not valid";
    }
}

function checkLastName(lastNameUser, fiscalCode) {
    var lastName = lastNameUser.split("");
    var found = [];
    var lastNameFC = fiscalCode.substring(0, 3).split("");
    lastName.forEach(element => {
        if(lastNameFC.includes(element.toUpperCase())){
            found.push(element);
        }
    });
    if(found.length>3 || found.length<3){
        throw "LastName is not valid";
    }
}

function checkYear(yearUser, fiscalCode) {
    var yearFC = fiscalCode.substring(6, 8);
    var year = yearUser.split("");
    var arr = [];
    arr.push(year[2]);
    arr.push(year[3]);
    arr.forEach(element => {
        if(!yearFC.includes(element)){
            throw "Year is not valid";
        }
    })
}

function checkMonth(monthUser, fiscalCode) {
    var monthCalculate = '';
    var dateFC = fiscalCode.substring(8, 9);
    var monthFC = '';

    switch (monthUser) {
        case '01': monthCalculate = "Gennaio"; break;
        case '02': monthCalculate = "Febbraio"; break;
        case '03': monthCalculate = "Marzo"; break;
        case '04': monthCalculate = "Aprile"; break;
        case '05': monthCalculate = "Maggio"; break;
        case '06': monthCalculate = "Giugno"; break;
        case '07': monthCalculate = "Luglio"; break;
        case '08': monthCalculate = "Agosto"; break;
        case '09': monthCalculate = "Settembre"; break;
        case '10': monthCalculate = "Ottobre"; break;
        case '11': monthCalculate = "Novembre"; break;
        case '12': monthCalculate = "Dicembre"; break;
        default: monthCalculate = undefined; break;
    }

    switch (dateFC) {
        case "A": monthFC = "Gennaio"; break;
        case "B": monthFC = "Febbraio"; break;
        case "C": monthFC = "Marzo"; break;
        case "D": monthFC = "Aprile"; break;
        case "E": monthFC = "Maggio"; break;
        case "H": monthFC = "Giugno"; break;
        case "L": monthFC = "Luglio"; break;
        case "M": monthFC = "Agosto"; break;
        case "P": monthFC = "Settembre"; break;
        case "R": monthFC = "Ottobre"; break;
        case "S": monthFC = "Novembre"; break;
        case "T": monthFC = "Dicembre"; break;
        default: monthFC = undefined; break;
    }
    if(monthCalculate !== monthFC){
        throw "Month is not valid";
    }
}

function checkDay(dayUser, fiscalCode) {
/*    var dayFC = fiscalCode.substring(9, 11);
    var day = dayUser.split("");
    var arr = [];
    arr.push(day[day.length -2]);
    arr.push(day[day.length -1]);
    arr.forEach(element => {
        if(!dayFC.includes(element)){
            throw "Day is not valid";
        }
    }) */
    var dateFC = fiscalCode.substring(9, 11);
    if (dateFC !== dayUser) {
        throw "Day is not valid";
    }
}

function checkData(user, fiscalCode) {
    const data = user.split("-");
    const year = data[0];
    const month = data[data.length - 2];
    const day = data[data.length - 1];
    checkYear(year, fiscalCode);
    checkMonth(month, fiscalCode);
    checkDay(day, fiscalCode);
}

function checkValability(user, fiscalCode) {
    try {
        if(fiscalCodeValidate(fiscalCode) == true){
            checkFirstName(user.FirstName, fiscalCode);
            checkLastName(user.LastName, fiscalCode);
            checkData(user.Birthdate, fiscalCode);
        } else {
            throw 'Fiscal Code is not valid';
        }
    } catch (error) {
        console.log(error);
        throw 'Error ' + error;
    }
}

export{
    checkValability
};