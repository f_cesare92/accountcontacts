import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {checkValability} from 'c/checkFiscalCode';

export default class FormContact extends LightningElement {

    @track contact = {Id: '', FirstName: '', LastName: '', Birthdate: '', Phone: '', Email: '', Fiscal_Code__c: '', AccountId: ''};
    @api contactdata;
    @track recordId;

    // called when the component is inserted in document
    connectedCallback() {
        //this.contact.Id = (this.contactdata != null && this.contactdata != "") ? this.contactdata.Id : "";
        this.editContact();
    }

    @api
    editContact(){
        console.log(JSON.stringify(this.contactdata));
        if(this.contactdata != ''){
            this.contact = this.contactdata;
            console.log(JSON.stringify(this.contact));
        }
    }

    deleteContact() {
        const onRemoveContact = new CustomEvent('deletecontactbyid', {detail: {ind:this.contactdata.Id}});
        this.dispatchEvent(onRemoveContact);
       // this.dispatchEvent(new CustomEvent('deletecontactbyid', {detail: {ind:this.contactdata.id}}));
    }

    @api
    getFieldsContact(){
        let clonedCnt = {...this.contact};
        try{
            const fields = this.template.querySelector("[data-id='form']");
            fields.querySelectorAll("lightning-input-field").forEach(obj => {
                if(obj.fieldName === 'Fiscal_Code__c'){
                    clonedCnt[obj.fieldName] = obj.value.toUpperCase();
                } else {
                    clonedCnt[obj.fieldName] = obj.value;
                }
            });
            checkValability(clonedCnt, clonedCnt.Fiscal_Code__c);
        } catch(error){
            const event = new ShowToastEvent({
                title: 'Error',
                message: error,
                variant: 'error',
            });
            this.dispatchEvent(event);
            throw 'error';
        }
        console.log(JSON.stringify(clonedCnt)); 
        return clonedCnt;
    }
/*
    @api
    setContactId(param){
        console.log(JSON.stringify(param));
        this.recordId = param.id;
        this.contact.Id = param.id;
        this.contact.AccountId = param.accountId;
        console.log('Contact: ' + JSON.stringify(this.contact));
    }
*/
    @api
    customValidateContact(){
        const val = [];
        this.template.querySelectorAll(".validate").forEach(obj => {
            if(obj.value === '') {
                val.push(obj.fieldName);
            }
        });
        return val;
    }

    /*
    fiscalCodeValidate(fiscalCode){
        const regex = /^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i;
        if(fiscalCode != ''){
            var ok = regex.exec(fiscalCode);
            if(!ok){
                return 'Fiscal_Code__c';
            }
        } else {
            return 'Fiscal_Code__c';
        }
    }
*/
}