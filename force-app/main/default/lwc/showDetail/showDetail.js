import { LightningElement, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

const actions = [
    { label: 'Edit', name: 'edit' },
    { label: 'Show details', name: 'show_details' }
];
const columns = [
    { label: 'Account Name', fieldName: 'Name' },
    { label: 'Phone', fieldName: 'Phone', type: 'phone' },
    { label: 'Website', fieldName: 'Website', type: 'url' },
    { label: 'Industry', fieldName: 'Industry' },
    { label: 'City', fieldName: 'BillingCity' },
    {
        type: 'action',
        typeAttributes: { rowActions: actions },
    },
];

export default class ShowDetail extends NavigationMixin(LightningElement) {

    @api result = [];
    columns = columns;
    recordId;
    
    handleRowAction(event) {
        const actionName = event.detail.action.name;
        //const row = event.detail.row;
        this.recordId = event.detail.row.Id;
        switch (actionName) {
            case 'edit':
                this.editRow();
                break;
            case 'show_details':
                this.showDetails();
                break;
            default:
        }
    }

    editRow(){
        const onEditRow = new CustomEvent('editrow', {detail: {ind:this.recordId}});
        this.dispatchEvent(onEditRow);
    }

    showDetails() {
        const onShowDetails = new CustomEvent('showdetails', {detail: {ind:this.recordId}});
        this.dispatchEvent(onShowDetails);
    }

/*
    editRow(){
        this[NavigationMixin.Navigate]({
            type : 'standard__navItemPage',
            attributes: {
                //recordId: this.account.Id,
                apiName: 'ManageForms'
            },
            state: {
                c__recordId: this.recordId
            }
        });
    }
*/
    
/*
    deleteRow(row) {
        const { id } = row;
        const index = this.findRowIndexById(id);
        if (index !== -1) {
            this.data = this.data
                .slice(0, index)
                .concat(this.data.slice(index + 1));
        }
    }
    
    findRowIndexById(id) {
        let ret = -1;
        this.data.some((row, index) => {
            if (row.id === id) {
                ret = index;
                return true;
            }
            return false;
        });
        return ret;
    }*/
}