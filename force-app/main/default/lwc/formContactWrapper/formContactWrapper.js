import { LightningElement, api, track } from 'lwc';

export default class FormContactWrapper extends LightningElement {

    @api items = [];
    @api contactsid;

    deleteContactById(event) {       
        const ind = event.detail.ind;
        const onRemoveContact = new CustomEvent('contactremove', {detail: {ind:ind}});
        this.dispatchEvent(onRemoveContact);       
        //this.dispatchEvent(new CustomEvent('contactremove', {detail: {ind:event.detail.ind}}));
    }

    @api
    getAllFields() { // result all forms
        const result = [];
        const element = this.template.querySelectorAll("c-form-contact");
        element.forEach(obj => {
            result.push(obj.getFieldsContact());
        });
        console.log('result: ' + JSON.stringify(result));
        return result;
    }
/*
    @api
    setAllId(param){
        const element = this.template.querySelectorAll("c-form-contact");
        this.items.forEach((obj, index) => {
            element[index].setContactId(param[index]);
        });
    }
*/
    @api
    customValidateAll() {
        const val = [];
        this.template.querySelectorAll("c-form-contact").forEach(obj => {
            if(obj.customValidateContact().length > 0){
                val.push(obj.customValidateContact());
            }
        });
        return val;
    }
}