import { LightningElement, track, api } from 'lwc';

export default class FormAccount extends LightningElement {
  
    //@track recordId;
    @track account = {Id: '', Name: '', Phone: '', Website: '', Industry: '', BillingStreet: '', BillingCity: '', BillingCountry: ''};
    @api accountid = '';
    @api accountedit = '';

    connectedCallback(){
        this.account.Id = (this.accountedit != null && this.accountedit != "") ? this.accountedit.Id : "";
    }

    @api
    editAccount(){
        console.log(JSON.stringify(this.accountedit));
        if(this.accountedit != ''){
            this.account = this.accountedit;
            console.log(JSON.stringify(this.account));
        }
    }

    @api
    resetForm(event){
        var inputFields = this.template.querySelectorAll('lightning-input-field');
        if(inputFields) {
            inputFields.forEach(field => {
                this.accountid = '';
                field.reset();
            });
        }
    }

    @api
    getFieldsAccount(){  
        let clonedAcc = {...(this.accountedit != "") ? this.accountedit : this.account};
        try {
            const fields = this.template.querySelector("[data-id='form']");
            fields.querySelectorAll('lightning-input-field').forEach(obj => {
                //console.log(obj.value);
                //this.account[obj.fieldName] = obj.value;
                clonedAcc[obj.fieldName] = obj.value;
            });
        } catch (error) {
            console.log(error);
        }
        //this.account.Id = this.accountid;
        console.log(JSON.stringify(clonedAcc));
        return clonedAcc;
    }

    @api
    customValidateAccount(){
        const val = [];
        this.template.querySelectorAll(".validate").forEach(obj => {
            if(obj.value == ''){
                val.push(obj.fieldName);
            }           
        });
        return val;
    }
}