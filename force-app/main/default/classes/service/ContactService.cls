public with sharing class ContactService {

    public ContactService() {}

    private ContactRepository contactRepository = new ContactRepository();

    public List<Contact> addContact(List<Contact> contacts, String accountId) {
        List<Contact> listCnt = new List<Contact>();
        for (Contact c : contacts) {
            c.AccountId = accountId;
            listCnt.add(c);
        }
        return addContact(listCnt);
    }

    public List<Contact> addContact(List<Contact> contacts) {
        List<Contact> listCnt = new List<Contact>();
        for (Contact c : contacts) {
            listCnt.add(addContact(c));
        }
        return listCnt;
    }

    public Contact addContact(Contact cnt, String accountId) {
        cnt.Id = null;
        cnt.AccountId = accountId;
        return addContact(cnt);
    }

    public Contact addContact(Contact cnt) {
        return contactRepository.saveContact(cnt);
    }

    public List<Contact> updateContact(List<Contact> contacts){
        List<Contact> listCnt = new List<Contact>();
        for(Contact c : contacts){
            listCnt.add(updateContact(c));
        }
        return listCnt;
    }

    public Contact updateContact(Contact cnt){
        contactRepository.updateContact(cnt);
        return cnt;
    }
    
    public void deleteContact(List<Contact> contacts){
        for(Contact c : contacts){
            deleteContact(c);
        }
    }

    public void deleteContact(Contact cnt){
        contactRepository.deleteContact(cnt);
    }

    public List<Contact> findContactByAccountId(String accountId){
        return contactRepository.findByAccountId(accountId);
    }

    public List<String> insertOrUpdateContact(List<Contact> contacts, Account acc){
        List<String> listId = new List<String>();
        String fit = 'fit';
            for(Contact c : contacts){
                String idContact = c.Id;
                if(idContact.contains(fit)){
//                if(String.isEmpty(c.Id)){
                    addContact(c, acc.Id);                   
                } else{
                    updateContact(c);
                }
                listId.add(c.Id);
            }
        return listId;
    }

    public List<Contact> manageContact(Map<String, String> param, Account acc){
        List<Contact> listCnt = new List<Contact>();       
        List<Contact> listJson = (List<Contact>) JSON.deserialize(param.get('contact'), List<Contact>.class);
        List<String> listContactsId = insertOrUpdateContact(listJson, acc);
        List<Contact> listDB = findContactByAccountId(acc.Id);
        if(listJson.size() > 0){
            for(Contact cnt : listDB){
                if(listContactsId.contains(cnt.Id)){
                    listCnt.add(cnt);
                } else {
                    deleteContact(cnt);
                }
            }
        } else {
            deleteContact(listDB);
        }
        return listCnt;
    }
}
