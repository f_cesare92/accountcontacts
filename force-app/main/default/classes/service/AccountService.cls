public with sharing class AccountService {
    
    public AccountService() {}

    private AccountRepository accountRepository = new AccountRepository();

    public List<Account> addAccount(List<Account> accounts){
        List<Account> listAcc = new List<Account>();
        for(Account a : accounts){
            listAcc.add(addAccount(a));
        }
        return listAcc;
    }

    public Account addAccount(Account acc){
        return accountRepository.saveAccount(acc);
    }

    public List<Account> updateAccount(List<Account> accounts){
        List<Account> listAcc = new List<Account>();
        for(Account a : accounts){
            listAcc.add(updateAccount(a));
        }
        return listAcc;
    }

    public Account updateAccount(Account acc){
        return accountRepository.updateAccount(acc);
    }

    public void deleteAccount(List<Account> accounts){
        for(Account a : accounts){
            deleteAccount(a);
        }
    }

    public void deleteAccount(Account acc){
        accountRepository.deleteAccount(acc);
    }

    public Account findAccountById(String accId){
        return accountRepository.findById(accId);
    }

    public List<Account> findAllAccounts(){
        return accountRepository.findAll();
    }

    public List<Account> findAccountByCondition(List<String> fields, Map<String, String> conditions){
        return accountRepository.findByCondition(fields, conditions);
    }

    public Account insertOrUpdateAccount(Map<String, String> param){
        Account acc = (Account) JSON.deserialize(param.get('account'), Account.class);
        if(acc.Id != null && !String.isBlank(acc.Id)) {
            acc = updateAccount(acc);
 
        } else {
            acc.Id = null;
            acc = addAccount(acc);
        }
        System.debug('account: ' + acc);
        return acc;
    }

    public Map<String, Object> manageAccountContact(Map<String, String> param){
        ContactService contactService = new ContactService();
        Account acc = insertOrUpdateAccount(param);
        List<Contact> listCnt = contactService.manageContact(param, acc);
        Map<String, Object> content = new Map<String, Object>();
        content.put('account', acc);
        content.put('contact', listCnt);
        System.debug('content: ' + content);
        return content;
    }

    public Map<String, Object> searchAccount(Map<String,String> param){
        Account acc = (Account) JSON.deserialize(param.get('account'), Account.class);
        List<String> fields = new List<String>();
        fields.add('Name');
        fields.add('Phone');
        fields.add('Website');
        fields.add('Industry');
        fields.add('BillingCity');
        Map<String, String> conditions = new Map<String, String>();
        conditions.put('Name', acc.Name);
        conditions.put('Industry', acc.Industry);
        List<Account> listAcc = findAccountByCondition(fields, conditions);
        Map<String, Object> content = new Map<String, Object>();
        content.put('account', listAcc);
        System.debug('content: ' + content);
        return content;
    }

    public Map<String, Object> getAll(){
        List<Account> accounts = findAllAccounts();
        Map<String, Object> content = new Map<String, Object>();
        content.put('account', accounts);
        System.debug('content: ' + content);
        return content;
    }

    public Map<String, Object> getAccountById(String accId){
        ContactService contactService = new ContactService();
        Account acc = findAccountById(accId);
        List<Contact> listCnt = contactService.findContactByAccountId(accId);
        Map<String, Object> content = new Map<String, Object>();
        content.put('account', acc);
        content.put('contact', listCnt);
        System.debug('content: ' + content);
        return content;
    }
}
