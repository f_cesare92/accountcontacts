public with sharing class ResponseDTO {

    @AuraEnabled
    public Map<String, Object> content;
    @AuraEnabled
    public String errorCode = '';
    @AuraEnabled
    public String errorMessage = '';
    @AuraEnabled
    public String message = '';
    @AuraEnabled
    public String messageCode = '';
    @AuraEnabled
    public Boolean status = false;

    public ResponseDTO() {}

    public Map<String, Object> getContent() {
        return content;
    }

    public void setContent(Map<String, Object> content) {
        this.content = content;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
