public with sharing class ManageFormsController {

    public ManageFormsController() {}

    private static AccountService accountService = new AccountService();
    private static ResponseDTO response = new ResponseDTO();
    
    @AuraEnabled
    public static ResponseDTO saveAll(Map<String, String> param){       
        try{
            response.content = accountService.manageAccountContact(param);
            response.status = true;
        } catch(Exception e){
            response.errorMessage = e.getMessage();
            response.errorCode = e.getStackTraceString();
        }
        System.debug('response: ' + response);
        return response;
    }

    @AuraEnabled
    public static ResponseDTO getAllAccounts(){
        try{
            response.content = accountService.getAll();
            response.status = true;
        } catch(Exception e){
            response.errorMessage = e.getMessage();
            response.errorCode = e.getStackTraceString();
        }
        System.debug('response: ' + response);
        return response;
    }

    @AuraEnabled
    public static ResponseDTO getAccounts(Map<String,String> param){
        try{
            response.content = accountService.searchAccount(param);
            response.status = true;
        } catch(Exception e){
            response.errorMessage = e.getMessage();
            response.errorCode = e.getStackTraceString();
        }
        System.debug('response: ' + response);
        return response;
    }

    @AuraEnabled
    public static ResponseDTO getId(String accId){
        try{
            response.content = accountService.getAccountById(accId);
            response.status = true;
        } catch(Exception e){
            response.errorMessage = e.getMessage();
            response.errorCode = e.getStackTraceString();
        }
        System.debug('response: ' + response);
        return response;
    }
    
    public class ApexException extends Exception {}
}
