public with sharing class ContactRepository {

    public ContactRepository() {}

    public List<Contact> findByAccountId (String accountId){
        return [SELECT Id, FirstName, LastName, Birthdate, Phone, Email, Fiscal_Code__c, AccountId FROM Contact WHERE AccountId = :accountId];
    }

    public Contact saveContact(Contact cnt){
        insert cnt;
        return cnt;
    }

    public Contact updateContact(Contact cnt){
        update cnt;
        return cnt;
    }

    public void deleteContact(Contact cnt){
        delete cnt;
    }
}
