public with sharing class AccountRepository {

    public AccountRepository() {}

    public Account findById(String accId){
        return [SELECT Id, Name, Phone, Website, Industry, BillingStreet, BillingCity, BillingCountry FROM Account WHERE Id = :accId];
    }

    public List<Account> findAll(){
        return [SELECT Name, Phone, Website, Industry, BillingStreet, BillingCity, BillingCountry FROM Account];
    }

    public Account saveAccount(Account acc){
        insert acc;
        return acc;
    }

    public Account updateAccount(Account acc){
        update acc;
        return acc;
    }

    public void deleteAccount(Account acc){
        delete acc;
    }

    public List<Account> findByCondition(List<String> fields, Map<String, String> conditions){
        String searchQuery = 'SELECT ';
        for(Integer i=0; i<fields.size(); i++){
            searchQuery += (i<fields.size()-1) ? fields.get(i) + ', ' : fields.get(i) + ' FROM Account WHERE ';
        }
        searchQuery += getConditionQuery(conditions);
        return Database.query(searchQuery);
    }

    private String getConditionQuery(Map<String, String> conditions) {
        String query = '';
        Integer i = 0;
        for(String key : conditions.keySet()){
            query += (String.isEmpty(conditions.get(key))) ? key + '!=\'Null\'': key + ' LIKE \'' + conditions.get(key) + '%\'';
            if (i+1 < conditions.size()){
                query += ' AND ';
            }
            i++;
        }
        return query;
    }
}
