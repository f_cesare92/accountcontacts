({
    init: function (component, event, helper) {
        var pageRef = component.get("v.pageReference");
        console.log('pageRef: ' + JSON.stringify(pageRef));
        if(pageRef != null){
            var recordId = pageRef.state.c__recordId;
            console.log('c__recordId: ' + recordId);
            var action = component.get("c.getId");
            action.setParams({
                accId: recordId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state == 'SUCCESS') {
                    var result = response.getReturnValue();
                    if(result.status){
                        console.log('result: ' + result);
                        try{
                            var account = result.content.account;
                            var listCnt = result.content.contact;
                            component.set("v.account", account);
                            component.set("v.contacts", listCnt);
                            var editAcc = component.find('account').editAccount();
                            //var editCnt = component.find('cntwrapper').editContacts();
                        } catch(e){
                            console.log('errore');
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }        
    },

    reInit: function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },

    addContact: function (component, event, helper) {
        var contacts = component.get("v.contacts");
/*        if (contacts.length > 0) {
            contacts.push({ id: (contacts[contacts.length - 1].id + 1) });
        } else {
            contacts.push({ id: contacts.length });
        } */
        contacts.push({Id: `fit${Date.now()}`});
        console.log(contacts);
        component.set("v.contacts", contacts);
    },

    removeContact: function (component, event, helper) {
        var contacts = component.get("v.contacts");
        console.log("controller remove id elemento: " + event.getParam('ind')); // prendo il valore dell' aura:attribute array
        for (var i = 0; i < contacts.length; i++) {
            console.log(contacts[i].Id);
            if (contacts[i].Id === event.getParam('ind')) {
                contacts.splice(contacts.indexOf(contacts[i]), 1);
            }
        }
        console.log(contacts);
        component.set("v.contacts", contacts); // nuovo valore dell' aura:attribute array
    },

    newAccount: function (component, event, helper) {
        var refresh = component.find('account').resetForm();
        component.set("v.contacts", []);
    },

    save: function (component, event, helper) {
        console.log('call save function');
        var account = component.find('account');
        var cntwrapper = component.find('cntwrapper')
        var accNotVal = account.customValidateAccount();
        var cntNotVal = cntwrapper.customValidateAll();
        if (accNotVal.length == 0 && cntNotVal.length == 0) {
            var record = {
                account: JSON.stringify(account.getFieldsAccount()),
                contact: JSON.stringify(cntwrapper.getAllFields())
            };
            console.log(JSON.stringify(record));
            var action = component.get("c.saveAll");
            action.setParams({
                param: record
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state == 'SUCCESS') {
                    var result = response.getReturnValue();
                    if(result.status){
                        console.log('result: ' + result);
                        try{
                            component.set("v.account", result.content.account);
                        } catch(e){}
                        var listCnt = result.content.contact;
                        if(listCnt.length > 0){
                           /* var listId = [];
                            for(var i=0; i<listCnt.length; i++){
                                var c = listCnt[i];
                                listId.push({
                                    id: c.Id,
                                    accountId: c.AccountId
                                });
                            } */
                            component.set("v.contacts", listCnt);
                        }
                        if(record.account.includes("Id")){
                            helper.messageAccount(component, event, helper, "Success!", "Account successfully edited.", "success");
                        } else {
                            helper.messageAccount(component, event, helper, "Success!", "Account successfully inserted.", "success");
                        }                       
                        //helper.closeFocusedTab(component, event, helper);
                    } else {
                        helper.messageAccount(component, event, helper, "Error!", "Error in inserting Account.", 'error');
                    }  
                } else {
                    helper.messageAccount(component, event, helper, "Error!", "Error in inserting Account.", 'error');
                }
            });
            $A.enqueueAction(action); 
        } else {
            helper.messageAccount(component, event, helper, "Error!", "Field is not completed!", 'error');
        }
    }
});