({
    helperMethod : function() {

    },

    getAllAccounts: function (component, event, helper){
        var action = component.get("c.getAllAccounts");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result.status) {
                    console.log(result.content.account);
                    try {
                        component.set("v.dataFromDb", result.content.account);
                    } catch (e) {
                        this.messageAccount(component, event, helper, 'Error', 'Error get Accounts', 'error');
                    }
                } else {
                    this.messageAccount(component, event, helper, 'Error', result, 'error');
                }
            } else {
                this.messageAccount(component, event, helper, 'Error', 'Server Error', 'error');
            }
        });
        $A.enqueueAction(action);
    },

    sendData: function (component, event, helper) {
        var data = {
            account: JSON.stringify(component.get("v.data"))
        };     
        console.log('data: ' + JSON.stringify(data));
        var action = component.get("c.getAccounts");
        action.setParams({
            param: data
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result.status) {
                    console.log(result.content.account);
                    try {
                        component.set("v.dataFromDb", result.content.account);
                    } catch (e) {
                        this.messageAccount(component, event, helper, 'Error', 'Error get Accounts', 'error');
                    }
                } else {
                    this.messageAccount(component, event, helper, 'Error', result, 'error');
                }
            } else {
                this.messageAccount(component, event, helper, 'Error', 'Server Error', 'error');
            }
        });
        $A.enqueueAction(action);
    },

    messageAccount: function(component, event, helper, title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },
})
