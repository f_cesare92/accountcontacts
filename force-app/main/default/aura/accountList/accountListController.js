({
    init: function(component, event, helper) {
    /*    var pageRef = {
            type : 'standard__component',
            attributes: {
                componentName: 'c__ManageForms'
            },
        };
        component.set("v.pageReference", pageRef); */
        helper.getAllAccounts(component, event, helper);
    },

    createAccount: function(component, event, helper) {
        var navService = component.find("navService");
        var pageRef = {
            type : 'standard__navItemPage',
            attributes: {
                apiName: 'ManageForms'
            },
        };
        navService.navigate(pageRef);
    },

    search: function (component, event, helper) {
        var eventName = event.getSource();
        var dataToSend = component.get("v.data", data);
        var name = eventName.get("v.name");
        var value = eventName.get("v.value");
        var data = {
            Name: '',
            Industry: ''
        };
        if(dataToSend != null){
            data = dataToSend;
        }
        if (value.length >= 3) {
            (name === "accountName") ? data.Name = value : data.Industry = value;
        } else {
            (name === "accountName") ? data.Name = '' : data.Industry = '';
        }
        component.set("v.data", data);
        console.log('data:' + JSON.stringify(data));
        helper.sendData(component, event, helper);
    },

    editRow: function (component, event, helper){
        var navService = component.find("navService");
        var accId = event.getParam('ind');
        console.log('accId: ' + accId);
/*        var pageRef = component.get("v.pageReference");
        pageRef.state ={c__recordId: accId}; */
        var pageRef = {
            type : 'standard__component',
            attributes: {
                componentName: 'c__ManageForms'
            },
            state: {
                c__recordId: accId
            }
        };
        console.log('pageRef: ' + JSON.stringify(pageRef));
        navService.navigate(pageRef);
    },

    showDetails: function (component, event, helper){
        var navService = component.find("navService");
        var accId = event.getParam('ind');
        var pageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accId,
                actionName: 'view'
            }
        };
        navService.navigate(pageRef);
    }
})
